<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();




Route::group( ['middleware' => 'auth' ], function()
{
    Route::get('/home', 'HomeController@index')->name('home');

    // Companies routes
    Route::prefix('companies')->group(function () {
    Route::get('', 'CompaniesController@index');
    Route::post('load', 'CompaniesController@load');
    Route::get('create', 'CompaniesController@create');
    Route::post('store', 'CompaniesController@store');
    Route::get("edit/{id}", "CompaniesController@edit");
    Route::post('update', 'CompaniesController@update');
    Route::post('delete', 'CompaniesController@delete');
    });

    // Employees routes
    Route::prefix('employees')->group(function () {
        Route::get('', 'EmployeeController@index')->name('employees.index'); 
        Route::post('load', 'EmployeeController@load');
        Route::get('create', 'EmployeeController@create');
        Route::post('store', 'EmployeeController@store')->name('employees.store');
        Route::get("edit/{id}", "EmployeeController@edit");
        Route::post('update', 'EmployeeController@update');
        Route::post('delete', 'EmployeeController@delete');
    });
});

//change language
Route::get('locale/{locale}', function($locale){
    App::setLocale($locale);
    //storing the locale in session to get it back in the middleware
    session()->put('locale', $locale);
    return redirect()->back();
});