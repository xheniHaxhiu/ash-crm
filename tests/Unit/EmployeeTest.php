<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Response;
use App\User;

class EmployeeTest extends TestCase
{
  
    /**
     *@covers::store
    *@test
    */
    public function teststoreEmployee()
    {
        // $url = route('employees.store');
        $requestParams = [
            'first_name'=>'Xheni',
            'last_name'=>'Haxhiu',
            'email'=> 'haxhiuxheni15@gmail.com',
            'company' => 1, // this should exist in database
            'phone' =>  '2021151',
        ];

        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response =  $this->post('/employees/store',  $requestParams); 
        $response->assertStatus( 200);
    }

    // Email not valid
    public function testShouldNotCreateEmployee()
    {
        $url = route('employees.store');
        $requestParams = [
            'first_name'=>'Xheni',
            'last_name'=>'Haxhiu',
            'email'=> 'haxhiuxheni15gmail.com',
            'company' => 1,
            'phone' =>  '2021151',
        ];
        $request = Request::create($url, 'POST', $requestParams);
        $controller = new EmployeeController();
        $response = $controller->store($request);

       $this->assertEquals(302, $response->getStatusCode());
    }
}
