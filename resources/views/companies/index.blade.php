@extends('layouts.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      @lang('app.companies')
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                <h3 class="box-title">@lang('app.manage')</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <table id="companies" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>@lang('app.name')</th>
                            <th>@lang('app.email')</th>
                            <th>@lang('app.website')</th>
                            <th>@lang('app.options')</th>
                        </tr>
                    </thead>
                </table>
                </div>
                <!-- /.box-body -->
            </div>
            </div>
            <!-- /.col -->
        </div>
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">@lang('app.delete') @lang('app.company')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    @lang('app.delete_company')
                    <form action="companies/delete" method="POST">
                        <input type="hidden" id="companyId" name="companyId" value="">              
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><span class='fa fa-close'></span> @lang('app.cancel')</button>
                    <button type="submit" class="btn btn-danger"><span class='fa fa-trash'></span> @lang('app.delete')</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('#companies').DataTable( {
                processing: true,
                serverSide: true,
                searchable: true,
                orderable: true,
                bDestroy : true,
                ajax: {
                    type: "post",
                    url: "/companies/load"
                },
                "columns": [
                    { "data": "name" },
                    { "data": "email" },
                    { "data": "website" },
                    { "data": "options",  "orderable": false }
                ]          
            } );
        });

        function onDeleteCompany(id) {
            $('#companyId').val(id);
        }
    </script>

@endsection