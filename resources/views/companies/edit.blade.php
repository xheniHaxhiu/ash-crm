@extends('layouts.template')
@section('custom-css')
    <style>
        #company-logo {
            margin-top:20px;
            width:100px;
            height: auto;
        }
    </style>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      @lang('app.edit') @lang('app.company')
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
            <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="/companies/update" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="email">@lang('app.name')</label>
                                <input type="text" class="form-control" name="name" value="{{ $company->name }}" required>
                            </div>
                            <div class="form-group">
                                <label for="password">@lang('app.email')</label>
                                <input type="email" class="form-control" name="email" value="{{ $company->email }}">
                            </div>
                            <div class="form-group">
                                <label for="website">@lang('app.website')</label>
                                <input type="text" class="form-control" name="website" value="{{ $company->website }}">
                            </div>
                            <div class="form-group">
                                <label for="logo">@lang('app.logo')</label>
                                <input type="file" name="logo" onchange="readURL(this);" accept="image/*">
                                <img src="{{asset('storage/img/'.$company->logo)}}" alt="" id="company-logo">
                            </div>
                            <input name="companyId" type="hidden" value="{{ $company->id }}">
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary"><span class='fa fa-save'></span> @lang('app.save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script>
        // File upload preview image
        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#company-logo')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
        }
    </script>
@endsection