@extends('layouts.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      @lang('app.add') @lang('app.company')
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
            <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="/companies/store" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="email">@lang('app.name')</label>
                                <input type="text" class="form-control" name="name" placeholder="@lang('app.name')" required>
                            </div>
                            <div class="form-group">
                                <label for="password">@lang('app.email')l</label>
                                <input type="email" class="form-control" name="email" placeholder="@lang('app.email')">
                            </div>
                            <div class="form-group">
                                <label for="website">@lang('app.website')</label>
                                <input type="text" class="form-control" name="website" placeholder="@lang('app.website')">
                            </div>
                            <div class="form-group">
                                <label for="logo">@lang('app.logo')</label>
                                <input type="file" name="logo" accept="image/*">
                            </div>
                            
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary"><span class='fa fa-save'></span> @lang('app.save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')

@endsection