@extends('layouts.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      @lang('app.employees')
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                <h3 class="box-title">@lang('app.manage')</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <table id="employees" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>@lang('app.first_name')</th>
                            <th>@lang('app.last_name')</th>
                            <th>@lang('app.company')</th>
                            <th>@lang('app.email')</th>
                            <th>@lang('app.phone')</th>
                            <th>@lang('app.options')</th>
                        </tr>
                    </thead>
                </table>
                </div>
                <!-- /.box-body -->
            </div>
            </div>
            <!-- /.col -->
        </div>
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">@lang('app.delete') @lang('app.employee')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                     @lang('app.delete_employee')
                    <form action="employees/delete" method="POST">
                        <input type="hidden" id="employeeId" name="employeeId" value="">              
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><span class='fa fa-close'></span> @lang('app.cancel')</button>
                    <button type="submit" class="btn btn-danger"><span class='fa fa-trash'></span> @lang('app.delete')</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('#employees').DataTable( {
                processing: true,
                serverSide: true,
                searchable: true,
                orderable: true,
                bDestroy : true,
                ajax: {
                    type: "post",
                    url: "/employees/load"
                },
                "columns": [
                    { "data": "first_name" },
                    { "data": "last_name" },
                    { "data": "company" },
                    { "data": "email" },
                    { "data": "phone" },
                    { "data": "options",  "orderable": false }
                ]          
            } );
        });

        function onDeleteEmployee(id) {
            $('#employeeId').val(id);
        }
    </script>

@endsection