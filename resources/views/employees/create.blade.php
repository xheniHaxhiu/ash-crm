@extends('layouts.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      @lang('app.add') @lang('app.employee')
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
            <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="/employees/store" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="first_name">@lang('app.first_name')*</label>
                                <input type="text" class="form-control" name="first_name" placeholder="@lang('app.first_name')" required>
                            </div>
                            <div class="form-group">
                                <label for="last_name">@lang('app.last_name')*</label>
                                <input type="text" class="form-control" name="last_name" placeholder="@lang('app.last_name')" required>
                            </div>
                            <div class="form-group">
                                <label for="company">@lang('app.company')</label>
                                <select class="form-control" id="company" name="company" >
                                    <option value="0" disabled selected>Select an option</option>
                                    <?php  
                                    for($i=0; $i< sizeof($companies); $i++) {  ?>
                                        <option value="<?php echo $companies[$i]->id ?>"><?php  echo $companies[$i]->name;  ?></option>      
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">@lang('app.email')</label>
                                <input type="email" class="form-control" name="email" placeholder="@lang('app.email')">
                            </div>
                            <div class="form-group">
                                <label for="phone">@lang('app.phone')</label>
                                <input type="text" class="form-control" name="phone" placeholder="@lang('app.phone')">
                            </div>
                            
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary"><span class='fa fa-save'></span> @lang('app.save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')

@endsection