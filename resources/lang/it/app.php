<?php
    return [
        'login' =>  'Accesso',
        'email' => 'Indirizzo Email',
        'password' => 'Password',
        'remember_me' =>'Ricordati di me',
        'forgot_pasw' => 'Hai dimenticato la password?',
        'companies' => 'Aziende',
        'manage' => 'Amministrare',
        'add' => 'Inserisci',
        'employees' => 'Dipendenti',
        'logout' => 'Disconnettersi',
        'home' => 'Casa',
        'name' => 'Nome',
        'website' => 'Sito Web',
        'options' => 'Opzioni',
        'company' => 'Azienda',
        'save' => 'Salvare',
        'logo' => 'Logo',
        'edit' => 'Modificare',
        'first_name' => 'Il nome',
        'last_name' => 'Cognome',
        'phone' => 'Telefono',
        'employee' => 'Dipendente',
        'delete_company' => 'Sei sicuro di voler eliminare questa compagnia?',
        'delete_employee' => 'Sei sicuro di voler eliminare questa dipendente?',
        'delete' => 'Elimina',
        'cancel' => 'Annulla',
        'created_company' => 'Hai creato con successo la compagnia.',
        'failed_creating_company' => 'La società non è riuscita a creare.',
        'updated_company' => 'Hai aggiornato con successo la compagnia.',
        'failed_deleting_company' => 'La società non è riuscita a eliminare.',
        'deleted_company' => 'Hai cancellato con successo Azienda.',
        'company_employee' => 'Ci sono impiegati in questa compagnia',
        'created_employee' => 'You have successfully created the dipediente.',
        'failed_creating_employee' => 'Dipendente failed to create.',
        'updated_employee' => 'You have successfully updated the Dipendente.',
        'failed_deleting_employee' => 'Dipendente failed to delete.',
        'deleted_employee' => 'You have successfully deleted the Dipendente.',
        'language' => 'Lingua',
        'english' => 'Inglese',
        'italian' => 'Italiano',
        'reset' => 'Reset',
        'reset_link' => 'Invia il link per reimpostare la password',
    ]
?>