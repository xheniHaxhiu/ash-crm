<?php

namespace App\Http\Middleware;
use Closure;
use Auth;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        //check here if the user is authenticated
        if ( ! Auth::user() )
        {
            // here you should redirect to login 
            return redirect()->guest('login');
        }
        return $next($request);
    }
}
