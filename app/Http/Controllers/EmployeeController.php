<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Company;
use Illuminate\Support\Facades\Validator;
use Session;

class EmployeeController extends Controller
{
    /**
     * Returns the view of employees
     */
    public function index()
    {
        return view('employees.index');
    }

    /**
     * Returns data for datatable
     */
    public function load(Request $request) 
    {
        $columns = array (
            0 =>'first_name',
            1 =>'last_name',
            2 =>'company',
            3 =>'email',
            4 =>'phone',
        );
        $start = $request->input ( 'start' );        
        $length = $request->input ( 'length' );   
        $orderColumn = $request->input ( 'order.0.column' );
        $orderDirection = $request->input ( 'order.0.dir' );  
        
        $totalData = Employee::count();                                                                                                                               
        $totalFiltered = $totalData; 
        if(empty($request->input('search.value')))
        {
            $employees = Employee::select(
                    'employees.id', 
                    'employees.first_name',
                    'employees.last_name', 
                    'companies.name', 
                    'employees.email', 
                    'employees.phone'
                )
                ->leftJoin('companies', 'companies.id', '=', 'employees.company_id')
                ->offset($start)
                ->limit($length)
                ->orderBy($columns [intval ( $orderColumn )], $orderDirection )
                ->get();
        }  
        else
        { 
            $searchValue = $request->input ( 'search.value' );
            $employees = Employee::select(
                        'employees.id', 
                        'employees.first_name',
                        'employees.last_name', 
                        'companies.name', 
                        'employees.email', 
                        'employees.phone'
                    )
                    ->leftJoin('companies', 'companies.id', '=', 'employees.company_id')
                    ->where( 'first_name', 'like', '%' . $searchValue . '%' )
                    ->where( 'last_name', 'like', '%' . $searchValue . '%' )
                    ->where( 'name', 'like', '%' . $searchValue . '%' )
                    ->orWhere('employees.email', 'like', '%' . $searchValue . '%')
                    ->orWhere('phone', 'like', '%' . $searchValue . '%')
                    ->offset($start)
                    ->limit($length)
                    ->orderBy($columns [intval ( $orderColumn )], $orderDirection )
                    ->get(); 
            $totalFiltered = $employees->count();
        } 

        $data = $this->prepareData($employees);       
        $tableContent = array (
            "draw" => intval ( $request->input ( 'draw' ) ), 
            "recordsTotal" => intval ( $totalData ), 
            "recordsFiltered" => intval ( $totalFiltered ), 
            "data" => $data
        );
        echo json_encode($tableContent);
    } 
    

    private function prepareData($employees)
    {
        $data = array ();
        foreach ( $employees as $employee ) {
            $nestedData = array ();
            $nestedData ["first_name"] = $employee->first_name;
            $nestedData ["last_name"] = $employee->last_name;
            $nestedData ["company"] = $employee->name;
            $nestedData ["email"] = $employee->email;
            $nestedData ["phone"] = $employee->phone;
            $nestedData["options"]="&emsp;<a title='EDIT' style='cursor:pointer' 
                href='/employees/edit/$employee->id'><span class='fa fa-edit' style='color:#007bff'  
                data-toggle='modal' data-target='#editModal'></span></a> &emsp;
                <a style='cursor:pointer' title='DELETE' onclick='onDeleteEmployee($employee->id)'>
                <span class='fa fa-trash' style='color:red' data-toggle='modal' data-target='#deleteModal'></span></a>";
            $data [] = $nestedData;
        };
        return $data;
    }

    /**
     * Return Add employee view
     */
    public function create(Request $request)
    {
        $companies = Company::get(); 
        return view('employees.create', compact('companies'));
    }

    /**
     * Store employee data
     */
    public function store(Request $request)
    {
        $employeeData = $request->all(); 
        $validator = Validator::make($employeeData, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'nullable|email|max:255',
            'phone' => 'nullable|string|max:255'
        ]);
        if ($validator->fails()) {           
            Session::flash('error', $validator->messages()->first());
            return redirect()->back();
        }

        if($request->has('company'))
            $company_id = $employeeData['company'];
        else
            $company_id = null;
        
        $employee = Employee::create([
            'first_name' => $employeeData['first_name'],
            'last_name' => $employeeData['last_name'],
            'company_id' => $company_id,
            'email' => $employeeData['email'],
            'phone' => $employeeData['phone']
        ]);

        if ($employee){
            Session::flash('success', __('app.created_employee'));
            return view('employees.index');
        }                                       
        else
        {
            Session::flash('error', __('app.failed_creating_employee'));
            return redirect()->back();
        }

    }


    /**
     * Return Edit employee view
     */
    public function edit($id)
    {
        $employee = Employee::where('id', $id)->first();
        $companies = Company::get(); 
        return view('employees.edit', compact('employee', 'companies'));
    }

    /**
     * Update employee data
     */
    public function update(Request $request)
    {
        $employeeData = $request->all();
        $validator = Validator::make($employeeData, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'nullable|email|max:255',
            'phone' => 'nullable|string|max:255'
        ]);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back();
        }

        if($request->has('company'))
            $company_id = $employeeData['company'];
        else
            $company_id = null;

        $employeeId = $employeeData['employeeId'];
        $employee = Employee::find($employeeId);
        // update the employee
        $employee->first_name = $employeeData['first_name'];
        $employee->last_name = $employeeData['last_name'];
        $employee->company_id = $company_id;
        $employee->email = $employeeData['email'];     
        $employee->phone = $employeeData['phone'];
        $employee->save();
        
        Session::flash('success',  __('app.updated_employee'));
        return redirect()->back();
    }


    /**
     * Delete employee
     */
    public function delete(Request $request)
    {
        $employeeId = $request->input('employeeId'); 
        $employee = Employee::find($employeeId); 
        if ($employee) {
            $employee->delete();
            Session::flash('success',  __('app.deleted_employee')); 
        }
        else
            Session::flash('error', __('app.failed_deleting_employee'));
        return redirect()->back();
    }

}
