<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Employee;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Upload;
use Session;
use App\Mail\CompanyCreated;
use Mail;

class CompaniesController extends Controller
{
    /**
     * Returns the view of companies
     */
    public function index(Request $request)
    {
        return view('companies.index');
    }

    /**
     * Returns data for datatable
     */
    public function load(Request $request) 
    {
        $columns = array (
            0 =>'name',
            1 =>'email',
            2 =>'website',
        );
        $start = $request->input ( 'start' );        
        $length = $request->input ( 'length' );   
        $orderColumn = $request->input ( 'order.0.column' );
        $orderDirection = $request->input ( 'order.0.dir' );  
        
        $totalData = Company::count();                                                                                                                               
        $totalFiltered = $totalData; 
        if(empty($request->input('search.value')))
        {
            $companies = Company::offset($start)
                ->limit($length)
                ->orderBy($columns [intval ( $orderColumn )], $orderDirection )
                ->get();
        }  
        else
        { 
            $searchValue = $request->input ( 'search.value' );
            $companies = Company::where( 'name', 'like', '%' . $searchValue . '%' )
                    ->orWhere('email', 'like', '%' . $searchValue . '%')
                    ->orWhere('website', 'like', '%' . $searchValue . '%')
                    ->offset($start)
                    ->limit($length)
                    ->orderBy($columns [intval ( $orderColumn )], $orderDirection )
                    ->get(); 
            $totalFiltered = $companies->count();
        } 

        $data = $this->prepareData($companies);       
        $tableContent = array (
            "draw" => intval ( $request->input ( 'draw' ) ), 
            "recordsTotal" => intval ( $totalData ), 
            "recordsFiltered" => intval ( $totalFiltered ), 
            "data" => $data
        );
        echo json_encode($tableContent);
    } 
    

    private function prepareData($companies)
    {
        $data = array ();
        foreach ( $companies as $company ) {
            $nestedData = array ();
            $nestedData ["name"] = $company->name;
            $nestedData ["email"] = $company->email;
            $nestedData ["website"] = $company->website;
            $nestedData["options"]="&emsp;<a title='EDIT' style='cursor:pointer' 
                href='/companies/edit/$company->id'><span class='fa fa-edit' style='color:#007bff'  
                data-toggle='modal' data-target='#editModal'></span></a> &emsp;
                <a style='cursor:pointer' title='DELETE' onclick='onDeleteCompany($company->id)'>
                <span class='fa fa-trash' style='color:red' data-toggle='modal' data-target='#deleteModal'></span></a>";
            $data [] = $nestedData;
        };
        return $data;
    }

    /**
     * Return Add company view
     */
    public function create(Request $request)
    {
        return view('companies.create');
    }

    /**
     * Store company data
     */
    public function store(Request $request)
    {
        $companyData = $request->all();
        $validator = Validator::make($companyData, [
            'name' => 'required|string|max:255',
            'email' => 'nullable|email|max:255',
            'website' => 'nullable|string|max:255',
            'logo' => 'image|dimensions:min_width=100,min_height=100'
        ]);
        if ($validator->fails()) {           
            Session::flash('error', $validator->messages()->first());
            return redirect()->back();
        }
        
        if ($request->hasFile('logo')) 
        {   
            $image = $request->file('logo');
            $image_name_new = $this->uploadImage($image);
        }
        else 
            $image_name_new = ''; 

        $company = Company::create([
            'name' => $companyData['name'],
            'email' => $companyData['email'],
            'logo' => $image_name_new,
            'website' => $companyData['website']
        ]);

        if ($company) 
        {
            $this->sendEmail($company->name);
            Session::flash('success', __('app.created_company')); 
        }                           
        else
            Session::flash('error', __('app.failed_creating_company'));
        return redirect()->back();
    }


    /**
     * Return Edit company view
     */
    public function edit($id)
    {
        $company = Company::where('id', $id)->first();
        return view('companies.edit', compact('company'));
    }

    /**
     * Update company data
     */
    public function update(Request $request)
    {
        $companyData = $request->all();
        $validator = Validator::make($companyData, [
            'name' => 'required|string|max:255',
            'email' => 'nullable|email|max:255',
            'website' => 'nullable|string|max:255',
            'logo' => 'image|dimensions:min_width=100,min_height=100'
        ]);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back();
        }

        $companyId = $companyData['companyId'];
        $company = Company::find($companyId);
        if ($request->hasFile('logo')) 
        {   
            $image = $request->file('logo');
            $image_name_new = $this->uploadImage($image); 
            $company->logo = $image_name_new;            
        }

        // update the company
        $company->name = $companyData['name'];
        $company->email = $companyData['email'];     
        $company->website = $companyData['website'];
        $company->save();

        Session::flash('success', __('app.updated_company'));
        return redirect()->back();
    }


    /**
     * Delete company
     */
    public function delete(Request $request)
    {
        $companyId = $request->input('companyId');
        $company = Company::find($companyId); 
        if ($company) {
            $employees = Employee::where('company_id', $companyId)->get();
            if(count($employees)>0)
            {
                Session::flash('error', __('app.company_employee'));
                return redirect()->back(); 
            }
            $company->delete();
            Session::flash('success', __('app.deleted_company')); 
        }
        else
            Session::flash('error', __('app.failed_deleting_company'));
        return redirect()->back();
    }

    

    // Function to upload image
    private function  uploadImage($image) 
    {
        $image_name_new = time().'_'.$image->getClientOriginalName(); 
        $image->storeAs(
            'img', $image_name_new, 'public'
        );
        // Storage::putFile('img', $image);
        // // Storage::disk('local')->putFileAs(
        // //     'public',
        // //     $image,
        // //     $image_name_new
        // // );
        return $image_name_new;
    }


    // Send Email when company is added
    public function sendEmail($companyName)
    {   
        Mail::to('xhenihaxhiu@ymail.com')->send(new CompanyCreated($companyName));
    }
}
